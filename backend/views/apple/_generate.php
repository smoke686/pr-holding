<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\AppleForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apple-generate">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <h3>Укажите колличество яблок для генерации</h3>
    <div class="row">
        <div class="col-md-10 col-xs-12">
            <?= $form->field($model, 'countApple')->input('number', ['placeholder' => 'Если не указывать, будет рандом от 1 до 100'])->label(false) ?>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <?= Html::submitButton('Сгенерировать', ['class' => 'btn btn-block btn-success pull-right']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
