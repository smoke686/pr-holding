<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AppleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $appleForm \common\models\AppleForm */

$this->title = 'Яблоки';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="apple-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <?php Pjax::begin(['id' => 'apple-pjaxList', 'enablePushState' => false]); ?>

        <?php echo $this->render('_generate', ['model' => $appleForm]); ?>
        <hr/>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'color',
                [
                    'attribute' => 'size',
                    'value' => function ($data) {
                        return \Yii::$app->formatter->asPercent($data->size);
                    }
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($data) {
                        /** @var $data \common\models\Apple */
                        $data->checkSpoiled();
                        return $data->getStatusName();
                    }
                ],
                'created_at:datetime',
                [
                    'attribute' => 'fall_date',
                    'value' => function ($data) {
                        /** @var $data \common\models\Apple */
                        return isset($data->fall_date) ? \Yii::$app->formatter->asDatetime($data->fall_date) : 'Яблоко еще не упало';
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{eat} {fall} {delete} ',
                    'buttons' => [
                        'eat' => function ($url, $model, $key) {
                            return Html::a(
                                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-resize-small']),
                                    ['#'],
                                    [
                                        'class' => 'eatAppleButton',
                                        'title' => 'Откусить кусок',
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#eatAppleModal',
                                            'url' => $url,
                                            'pjax' => '0'
                                        ],
                                    ]
                                );
                        },
                        'fall' => function ($url, $model, $key) {
                            return Html::a(
                                Html::tag('span', '', ['class' => 'glyphicon glyphicon-hand-down', 'data-method' => 'post']),
                                [$url],
                                ['title' => 'Уронить', 'data-pjax' => '0']
                            );
                        }
                    ],
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>
        <div class="modal" id="eatAppleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Сколько откусить</h3>
                    </div>
                    <div class="modal-body">
                        <?php echo $this->render('_eat', ['model' => $appleForm]); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php $this->registerJs(<<<JS
    $('body').on('click', '.eatAppleButton', function(){
        ths = $(this);
        $('#eatForm').attr('action', ths.data('url'));
        return true;
    });
JS
);