<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\AppleForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apple-eat">

    <?php $form = ActiveForm::begin([
        'action' => ['eat'],
        'id' => 'eatForm',
        'options' => [
            'data-pjax' => 0
        ],
    ]); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-9 col-xs-12">
                    <?= $form->field($model, 'eat')
                        ->input('number', [
                            'placeholder' => 'Сколько откусить',
                            'min' => 0,
                            'required' => 'required'
                        ])
                        ->label(false) ?>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <?= Html::submitButton('Откусить', ['class' => 'btn btn-block btn-success pull-right']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
