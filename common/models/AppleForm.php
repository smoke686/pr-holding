<?php
/**
 * Created by PhpStorm.
 * User: symbian
 * Date: 03.09.19
 * Time: 20:56
 */

namespace common\models;


use common\exceptions\AppleException;
use yii\base\Model;

/**
 * Class AppleForm
 * @package common\models
 *
 * @property int $countApple
 * @property float $eat
 */
class AppleForm extends Model
{
    public $countApple = 0;
    public $eat;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countApple'], 'integer'],
            [['eat'], 'number'],
        ];
    }

    public static function getColorList()
    {
        return [
            'red',
            'orange',
            'yellow',
            'green',
            'blue',
            'purple',
            'pink',
            'black',
            'white',
            'gray',
            'brown'
        ];
    }

    /**
     *  Генерация Яблок
     *
     * @param array $colorList
     * @throws \common\exceptions\AppleException
     */
    public function generateApple($colorList = [])
    {
        if(empty($colorList)){
            $colorList = static::getColorList();
        }

        $transaction = \Yii::$app->db->beginTransaction();
        if (!$transaction) {
            return;
        }
        try {
            $count = (int)$this->countApple !== 0 ? $this->countApple : rand(1, 100);
            for ($i = 0; $i < $count; $i++) {
                $apple = new Apple($colorList[array_rand($colorList)]);
                $apple->save();
            }
            $transaction->commit();
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw new AppleException('Ошибка генерации');
        }
    }

    /**
     * @param Apple $model
     * @return bool
     * @throws \common\exceptions\AppleException
     */
    public function eatApple(Apple $model)
    {
        if (!$this->eat) {
            return false;
        }
        return $model->eat($this->eat);
    }
}