<?php

namespace common\models;

use common\exceptions\AppleException;
use Yii;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property string $color Цвет
 * @property string $size Целостность яблока
 * @property int $status Статус
 * @property int $fall_date Дата падения
 * @property int $created_at Дата появления
 */
class Apple extends \yii\db\ActiveRecord
{
    const STATUS_HANGING = 1; //Яблоко висит
    const STATUS_FALL = 2; //Яблоко упало
    const STATUS_SPOILED = 3; //Яблоко испортилось

    private $_timeBeforeAppleSpoiled = 18000; //Дефолтное время в (секундах) пока не испортилось яблоко 5 часов

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['size'], 'number'],
            [['status', 'fall_date', 'created_at'], 'integer'],
            [['color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Цвет',
            'size' => 'Целостность яблока',
            'status' => 'Статус',
            'fall_date' => 'Дата падения',
            'created_at' => 'Дата появления',
        ];
    }

    /**
     * Apple constructor.
     * Перадать или массив данных или название цвета
     *
     * @param string|array $config
     */
    public function __construct($config = [])
    {
        if (!is_array($config)) {
            $config = [
                'color' => $config ?? ''
            ];
        }

        //А вот это вот костыль. Не надо так, но уже поздно переписывать.
        //Данное решение работает как через веб, так и через "примеры вызова" которые были в ТЗ
        if (isset($config['color'])) {
            $this->status = static::STATUS_HANGING;
            $this->size = 1;
            $this->created_at = rand(0, mktime());
        }

        parent::__construct($config);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $save = parent::beforeSave($insert);

        //Инициализация
        if ($insert) {
            $this->status = $this->status ?? static::STATUS_HANGING;
            $this->size = $this->size ?? 1;
            $this->created_at = $this->created_at ?? rand(0, mktime());
        }

        return $save;
    }

    /**
     * Установка времени (в секундах), после которого упавшее яблоко считается испорченым
     *
     * @param int $value
     */
    public function setTimeBeforeAppleSpoiled(int $value)
    {
        $this->_timeBeforeAppleSpoiled = $value;
    }

    /**
     * Статусы яблока
     *
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            static::STATUS_HANGING => 'Висит на дереве',
            static::STATUS_FALL => 'Упало/лежит на земле',
            static::STATUS_SPOILED => 'Гнилое яблоко',
        ];
    }

    /**
     * Получить статус яблока
     *
     * @return string
     */
    public function getStatusName()
    {
        return static::getStatusArray()[$this->status] ?? static::getStatusArray()[static::STATUS_HANGING];
    }

    /**
     * Проверка яблока на съедобность
     *
     * @return bool
     */
    public function checkSpoiled()
    {
        if ($this->status == static::STATUS_SPOILED) {
            return true;
        }

        if ($this->fall_date && $this->fall_date + $this->_timeBeforeAppleSpoiled < mktime()) {
            $this->status = static::STATUS_SPOILED;
        }

        return $this->status == static::STATUS_SPOILED;
    }

    /**
     * Откусить кусок
     *
     * @param float $size
     * @return float|int|string
     * @throws AppleException
     */
    public function eat(float $size)
    {
        if ($size < 0 || $size > 100) {
            throw new AppleException('Кусок вышел за рамки яблока');
        }

        switch ($this->status) {
            case static::STATUS_HANGING:
                throw new AppleException('Съесть нельзя, яблоко на дереве');
                break;
            case $this->checkSpoiled():
                throw new AppleException('Съесть нельзя, яблоко испортилось');
                break;
            default:
                $afterSize = $this->size * 100 - $size;
                if ($afterSize >= 0) {
                    $this->size = $afterSize / 100;
                } else {
                    throw new AppleException('Нельзя съесть больше чем есть');
                }
        }

        return $this->size;
    }

    /**
     * Уронить яблоко
     *
     * @throws AppleException
     */
    public function fallToGround()
    {
        if ($this->status !== static::STATUS_HANGING) {
            throw new AppleException('Яблоко уже упало');
        }
        $this->status = static::STATUS_FALL;
        $this->fall_date = mktime();
    }
}
