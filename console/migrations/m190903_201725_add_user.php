<?php

use yii\db\Migration;

/**
 * Class m190903_201725_add_user
 */
class m190903_201725_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'username' => 'apple',
            'password_hash' => '$2y$13$.RGZzx6.DdRz.kmv9i7jLONtARiZhVNDwPa.19gP4BxgK86rGtVyq',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'email' => 'apple@example.com',
            'status' => 10,
            'created_at' => 1567541798,
            'updated_at' => 1567541798
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['username' => 'apple']);
    }
}
