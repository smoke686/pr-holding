<?php

use yii\db\Migration;

/**
 * Class m190903_164940_create_apple
 */
class m190903_164940_create_apple extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apple}}', [
            'id' => $this->primaryKey(),
            'color' => $this->string(255)->null()->comment('Цвет'),
            'size' => $this->float()->null()->comment('Целостность яблока'),
            'status' => $this->integer(1)->null()->comment('Статус'),
            'fall_date' => $this->integer()->null()->comment('Дата падения'),
            'created_at' => $this->integer()->null()->comment('Дата появления'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apple}}');
    }
}
